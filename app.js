require('dotenv').config();
const express = require('express');
const helmet = require('helmet');
const weatherController = require('./controllers/weathercontroller');

const app = express();
app.use(helmet());

app.get("/forecast", weatherController.getWeatherNow);

app.listen(process.env.SERVER_PORT, () => console.log(`App started on port ${process.env.SERVER_PORT}`));

weatherController.updateWeather();
setInterval(weatherController.updateWeather, 1600000);