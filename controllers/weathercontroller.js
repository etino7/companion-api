const axios = require("axios");
require("dotenv").config();
const redis = require("redis");
const client = redis.createClient();

const KEY = process.env.DARK_SKY_API_KEY;
const API_URL = `https://api.darksky.net/forecast/${KEY}/43.5112,16.4692?lang=hr&units=ca`;

module.exports = {
  async getWeatherNow(req, res) {
    client.get("weather_now", function(err, reply) {
      res.status(200).json(reply);
      console.log("requests served"); 
    });
    client.incr("requests_served");
  },
  async updateWeather() {
    try {
      let response = await axios.get(API_URL);
      client.set("weather_now", JSON.stringify(response.data));
      console.log("weather updated");
    } catch (er) {
      console.log(er);
    }
  }
};
